import 'package:flutter/material.dart';

class DrawerScreen extends StatelessWidget {
  const DrawerScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: Text("Ismail Wibiwicaksono"),
              currentAccountPicture:
              CircleAvatar(backgroundImage: AssetImage("assets/img/ismail.jpg")),
              accountEmail: Text("wibidjokam@gmail.com"),
            ),
            DrawerListTile(
              iconData: Icons.home,
              title: "Home",
              onTilePressed: () {
                Navigator.pushNamed(context, '/home');
              },
            ),
            DrawerListTile(
              iconData: Icons.person,
              title: "Profile",
              onTilePressed: () {
                Navigator.pushNamed(context, '/profile');
              },
            ),
            DrawerListTile(
              iconData: Icons.logout,
              title: "Logout",
              onTilePressed: () {
                Navigator.pushNamed(context, '/login');
              },
            ),
          ],
        )
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final IconData? iconData;
  final String? title;
  final VoidCallback? onTilePressed;

  const DrawerListTile(
      {Key? key, this.iconData, this.title, this.onTilePressed})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      dense: true,
      leading: Icon(iconData),
      title: Text(
        title!,
        style: TextStyle(fontSize: 16),
      ),
    );
  }
}